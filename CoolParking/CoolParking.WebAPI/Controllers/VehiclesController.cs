﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IList<Vehicle>> GetVehicles()
        {
            return _parkingService.GetVehicles();
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById([RegularExpression(Settings.VehicleIdValidationPattern,
                ErrorMessage = "ID must match pattern {1}")]
            string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle is null) return NotFound(new {Error = "Non existing vehicle"});

            return vehicle;
        }

        [HttpPost]
        public ActionResult AddVehicle(VehicleVm vehicle)
        {
            try
            {
                _parkingService.AddVehicle(new Vehicle(vehicle.Id, (VehicleType) vehicle.VehicleType, vehicle.Balance));
            }
            catch (Exception e)
            {
                return BadRequest(new {Error = e.Message});
            }

            return Created($"/api/vehicles/{vehicle.Id}", vehicle);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(
            [RegularExpression(Settings.VehicleIdValidationPattern,
                ErrorMessage = "ID must match pattern {1}")]
            string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle is null) return NotFound(new {Error = "Non existing vehicle"});

            try
            {
                _parkingService.RemoveVehicle(vehicle.Id);
            }
            catch (Exception e)
            {
                return BadRequest(new {Error = e.Message});
            }

            return NoContent();
        }
    }
}
