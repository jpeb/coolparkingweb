﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult<IList<TransactionInfo>> GetLastTransactions()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            string log;

            try
            {
                log = _parkingService.ReadFromLog();
            }
            catch (Exception e)
            {
                return BadRequest(new { Error = e.Message });
            }

            return log;
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> GetTopUpVehicle(TopUpVehicleVm topUp)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == topUp.Id);

            if (vehicle is null) return NotFound(new {Error = "Non existing vehicle"});

            try
            {
                _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);
            }
            catch (Exception e)
            {
                return BadRequest(new {Error = e.Message});
            }

            return vehicle;
        }
    }
}
