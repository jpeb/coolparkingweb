﻿using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.ViewModels
{
    public class VehicleVm
    {
        [Required(ErrorMessage = "Vehicle ID is required")]
        [RegularExpression(Settings.VehicleIdValidationPattern, ErrorMessage = "ID must match pattern {1}")]
        public string Id { get; set; }

        [Range(0, 3, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public int VehicleType { get; set; }

        [Range(double.Epsilon, double.PositiveInfinity, ErrorMessage = "Balance must be positive")]
        public decimal Balance { get; set; }
    }
}
