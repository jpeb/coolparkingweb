﻿using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.ViewModels
{
    public class TopUpVehicleVm
    {
        [Required(ErrorMessage = "Vehicle ID is required")]
        [RegularExpression(Settings.VehicleIdValidationPattern, ErrorMessage = "ID must match pattern {1}")]
        public string Id { get; set; }

        [Range(double.Epsilon, double.PositiveInfinity, ErrorMessage = "Top up sum must be positive")]
        public decimal Sum { get; set; }
    }
}
