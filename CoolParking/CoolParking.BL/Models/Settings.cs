﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialParkingBalance = 0;
        public const int ParkingCapacity = 10;
        public const int PaymentChargePeriod = 5;
        public const int LoggingPeriod = 60;
        public const decimal PenaltyRate = 2.5m;

        // ID Format
        // ХХ-YYYY-XX (where X is any letter of the english alphabet in uppercase and Y is any digit)
        public const string VehicleIdValidationPattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

        // The tariffs are depending on the vehicle type
        public static readonly ReadOnlyDictionary<VehicleType, decimal> Tariffs
            = new(new Dictionary<VehicleType, decimal>
            {
                {VehicleType.PassengerCar, 2},
                {VehicleType.Truck, 5},
                {VehicleType.Bus, 3.5m},
                {VehicleType.Motorcycle, 1}
            });
    }
}
