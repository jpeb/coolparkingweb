﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ILogService _logService;
        private readonly ITimerService _logTimer;
        private readonly ITimerService _withdrawTimer;
        private readonly Parking _parking;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _parking = Parking.GetInstance();

            withdrawTimer.Interval = Settings.PaymentChargePeriod * 1000;
            logTimer.Interval = Settings.LoggingPeriod * 1000;

            withdrawTimer.Elapsed += OnWithdrawTimerElapsed;
            logTimer.Elapsed += OnLogTimerElapsed;

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public IList<TransactionInfo> LastParkingTransactions { get; set; } = new List<TransactionInfo>();

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
                throw new ArgumentException($"Vehicle with ID = '{vehicle.Id}' is already in the parking");

            if (_parking.Vehicles.Count == _parking.Capacity)
                throw new InvalidOperationException("The parking is full");

            _parking.Vehicles.Add(vehicle);
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Non existing vehicle");

            if (vehicle.Balance < 0)
                throw new InvalidOperationException("Debt collectors are coming ;)");

            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Sum cannot be negative");

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Non existing vehicle");

            vehicle.Balance += sum;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return LastParkingTransactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void Dispose()
        {
            _parking.Vehicles.Clear();
            _parking.Balance = Settings.InitialParkingBalance;
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        private void OnLogTimerElapsed(object obj, System.Timers.ElapsedEventArgs args)
        {
            _logService.Write(string.Join(Environment.NewLine, LastParkingTransactions));
            LastParkingTransactions.Clear();
        }

        private void OnWithdrawTimerElapsed(object obj, System.Timers.ElapsedEventArgs args)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                var tariff = Settings.Tariffs[vehicle.VehicleType];
                var balance = vehicle.Balance;

                decimal tax;

                if (balance > 0)
                    tax = balance >= tariff
                        ? tariff
                        : Settings.PenaltyRate * (tariff - balance) + balance;
                else
                    tax = tariff * Settings.PenaltyRate;

                vehicle.Balance -= tax;
                _parking.Balance += tax;

                LastParkingTransactions.Add(new TransactionInfo(DateTime.Now, vehicle.Id, tax));
            }
        }
    }
}
