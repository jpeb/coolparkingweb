﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public string LogPath { get; }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException($"File '{LogPath}' not found");

            return File.ReadAllText(LogPath);
        }

        public void Write(string logInfo)
        {
            File.AppendAllText(LogPath, logInfo + Environment.NewLine);
        }
    }
}
