﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using CoolParking.ConsoleUI.Configuration;
using CoolParking.ConsoleUI.Menu;
using CoolParking.ConsoleUI.Services;
using CoolParking.ConsoleUI.Tables;

namespace CoolParking.ConsoleUI
{
    internal class Program
    {
        private const string Title = @"
   _____            _   _____           _    _             
  / ____|          | | |  __ \         | |  (_)            
 | |     ___   ___ | | | |__) |_ _ _ __| | ___ _ __   __ _ 
 | |    / _ \ / _ \| | |  ___/ _` | '__| |/ / | '_ \ / _` |
 | |___| (_) | (_) | | | |  | (_| | |  |   <| | | | | (_| |
  \_____\___/ \___/|_| |_|   \__,_|_|  |_|\_\_|_| |_|\__, |
                                                      __/ |
   Use arrows and Enter for navigation...            |___/ 
   
";

        private static void Main(string[] args)
        {
            Console.Title = "CoolParking";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var client = new HttpClient
            {
                BaseAddress = new Uri(ApiUris.BaseAddress)
            };

            var app = new AppController(
                new ParkingWebService(client),
                new VehicleTableFormatter(),
                new TransactionTableFormatter()
            );

            List<MenuOption> options = new()
            {
                new MenuOption("Вывести текущий баланс парковки", app.ShowCurrentParkingBalance),
                new MenuOption("Вывести сумму заработанных денег за текущий период", app.ShowEarnedMoney),
                new MenuOption("Вывести количество свободных/занятых мест на парковке", app.ShowFreeOccupiedPlaces),
                new MenuOption("Вывести на экран все транзакции парковки за текущий период",
                    app.ShowLastParkingTransactions),
                new MenuOption("Вывести историю транзакций из файла", app.ShowParkingTransactionsFromLog),
                new MenuOption("Вывести список транспортных средств находящихся на паркинге",
                    app.ShowVehiclesInParking),
                new MenuOption("Поставить транспортное средство на паркинг", app.AddVehicle),
                new MenuOption("Забрать транспортное средство с паркинга", app.RemoveVehicleFromParking),
                new MenuOption("Пополнить баланс конкретного транспортного средства", app.TopUpVehicle),
                new MenuOption("Выход", () =>
                {
                    app.Dispose();
                    Environment.Exit(0);
                })
            };

            var menu = new ConsoleMenu(options, Title)
            {
                PrimaryColor = (ConsoleColor.White, ConsoleColor.Black),
                TitleColor = (ConsoleColor.Red, ConsoleColor.Black),
                SelectedColor = (ConsoleColor.Black, ConsoleColor.DarkGreen)
            };

            menu.RunMenu();
        }
    }
}
