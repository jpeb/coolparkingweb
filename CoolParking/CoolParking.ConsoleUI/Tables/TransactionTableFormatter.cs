﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking.ConsoleUI.Tables
{
    internal class TransactionTableFormatter : ITableFormatter<TransactionInfo>
    {
        private const string TableTop =
            "┌────────────────────────┬────────────────────────┬─────────────┐\n" +
            "│       VEHICLE ID       │      DATE AND TIME     │     SUM     │\n" +
            "├────────────────────────┼────────────────────────┼─────────────┤\n";

        private const string TableBottom =
            "└────────────────────────┴────────────────────────┴─────────────┘\n";

        public string Format(ICollection<TransactionInfo> collection)
        {
            var sb = new StringBuilder();

            sb.Append(TableTop);

            foreach (var vehicle in collection)
                sb.Append($"│ {vehicle.VehicleId,-22} │ {vehicle.DateTime,-22} │ {vehicle.Sum,-11} │\n");

            sb.Append(TableBottom);

            return sb.ToString();
        }
    }
}
