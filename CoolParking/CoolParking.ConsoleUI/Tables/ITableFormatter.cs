﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Tables
{
    internal interface ITableFormatter<T>
    {
        string Format(ICollection<T> collection);
    }
}
