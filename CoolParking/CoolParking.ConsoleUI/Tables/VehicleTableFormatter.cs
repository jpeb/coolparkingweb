﻿using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Models;

namespace CoolParking.ConsoleUI.Tables
{
    internal class VehicleTableFormatter : ITableFormatter<Vehicle>
    {
        private const string TableTop = 
            "┌────────────────────────┬────────────────────────┬─────────────┐\n" + 
            "│       VEHICLE ID       │      VEHICLE TYPE      │   BALANCE   │\n" + 
            "├────────────────────────┼────────────────────────┼─────────────┤\n";

        private const string TableBottom = 
            "└────────────────────────┴────────────────────────┴─────────────┘\n";

        public string Format(ICollection<Vehicle> collection)
        {
            var sb = new StringBuilder();

            sb.Append(TableTop);

            foreach (var vehicle in collection)
                sb.Append($"│ {vehicle.Id,-22} │ {vehicle.VehicleType,-22} │ {vehicle.Balance,-11} │\n");

            sb.Append(TableBottom);

            return sb.ToString();
        }
    }
}