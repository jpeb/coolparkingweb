﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.ConsoleUI.Configuration
{
    public static class ApiUris
    {
        public const string BaseAddress = "https://localhost:5001/";

        // Parking
        public const string GetParkingBalance = "api/parking/balance";
        public const string GetParkingCapacity = "api/parking/capacity";
        public const string GetParkingFreePlaces = "api/parking/freePlaces";

        // Vehicles
        public const string GetVehicles = "api/vehicles";
        public const string PostVehicles = "api/vehicles";
        public const string DeleteVehicles = "api/vehicles";
        public const string GetVehicle = "api/vehicles";

        // Transactions
        public const string GetAllTransactions = "api/transactions/all";
        public const string GetLastTransactions = "api/transactions/last";
        public const string PostTopUpVehicle = "api/transactions/topUpVehicle";
    }
}
