﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.ConsoleUI.Configuration;

namespace CoolParking.ConsoleUI.Services
{
    internal class ParkingWebService : IParkingService
    {
        private readonly HttpClient _httpClient;

        public ParkingWebService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        public decimal GetBalance()
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Get,
                    ApiUris.GetParkingBalance)
            );

            response.EnsureSuccessStatusCode();

            var balance = decimal.Parse(
                response.Content.ReadAsStringAsync().Result,
                CultureInfo.InvariantCulture
            );

            return balance;
        }

        public int GetCapacity()
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Get,
                    ApiUris.GetParkingCapacity)
            );

            response.EnsureSuccessStatusCode();

            var capacity = int.Parse(response.Content.ReadAsStringAsync().Result);

            return capacity;
        }

        public int GetFreePlaces()
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Get,
                    ApiUris.GetParkingFreePlaces)
            );

            response.EnsureSuccessStatusCode();

            var freePlaces = int.Parse(response.Content.ReadAsStringAsync().Result);

            return freePlaces;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Get,
                    ApiUris.GetVehicles)
            );

            response.EnsureSuccessStatusCode();

            var vehicles =
                response.Content.ReadFromJsonAsync<List<Vehicle>>().Result ??
                throw new InvalidOperationException("Error when getting the collection of vehicles");

            return new ReadOnlyCollection<Vehicle>(vehicles.ToList()
            );
        }

        public void AddVehicle(Vehicle vehicle)
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Post,
                    ApiUris.PostVehicles)
                {
                    Content = new StringContent(
                        JsonSerializer.Serialize(vehicle),
                        Encoding.UTF8,
                        "application/json"
                    )
                }
            );

            response.EnsureSuccessStatusCode();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Delete,
                    $"{ApiUris.DeleteVehicles}/{vehicleId}")
            );

            response.EnsureSuccessStatusCode();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Put,
                    ApiUris.PostTopUpVehicle)
                {
                    Content = new StringContent(
                        JsonSerializer.Serialize(new {Id = vehicleId, Sum = sum}),
                        Encoding.UTF8,
                        "application/json"
                    )
                }
            );

            response.EnsureSuccessStatusCode();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Get,
                    ApiUris.GetLastTransactions)
            );

            response.EnsureSuccessStatusCode();

            var lastTransactions =
                response.Content.ReadFromJsonAsync<TransactionInfo[]>().Result ??
                throw new InvalidOperationException("Error when getting the collection of transactions");

            return lastTransactions;
        }

        public string ReadFromLog()
        {
            var response = _httpClient.Send(
                new HttpRequestMessage(HttpMethod.Get,
                    ApiUris.GetAllTransactions)
            );

            response.EnsureSuccessStatusCode();

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
